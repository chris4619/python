'''
Created on 2019-4-15

@author: YangYang
'''
from graphviz import Digraph
import tensorflow as tf


def tf_to_dot(graph):
    dot = Digraph()
    for n in g.as_graph_def().node:
        dot.node(n.name, labels=n.name)
        for i in n.input:
            dot.edge(i, n.name)
    return dot


g = tf.Graph()
with g.as_default():
    X = tf.placeholder(tf.float32, name='X')

    W1 = tf.placeholder(tf.float32, name='W1')
    b1 = tf.placeholder(tf.float32, name='b1')    
    a1 = tf.nn.relu(tf.matmul(X, W1) + b1)

    W2 = tf.placeholder(tf.float32, name='W2')
    b2 = tf.placeholder(tf.float32, name='b2')
    a2 = tf.nn.relu(tf.matmul(a1, W2) + b2)

    W3 = tf.placeholder(tf.float32, name='W3')
    b3 = tf.placeholder(tf.float32, name='b3')
    y_hat = tf.matmul(a2, W3) + b3
tf_to_dot(g)
