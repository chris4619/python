'''
Created on 2019��4��11��

@author: YangYang
'''
# -*- coding: utf-8 -*-

# 引入模块
import tensorflow as tf
# 定义一个张量等于[1.0,2.0]
a = tf.constant([1.0, 2.0])
# 定义一个张量等于[3.0,4.0]
b = tf.constant([3.0, 4.0])
# 实现a加b的加法 
result = a + b
# 打印出结果 可以打印出这样一句话：
# Tensor(“add:0”, shape=(2, ), dtype=float32)
# 意思为result是一个名称为add:0的张量，shape=(2,)表示一维数组长度为2,dtype=float32表示数据类型为浮点型。 
print (result)
#执行会话并打印出执行后的结果 
#可以打印出这样的结果：[[11.]]
with tf.Session() as sess:
    print (sess.run(result))
