# -*- coding: utf-8 -*-
'''
Created on 2019年4月11日

@author: YangYang
'''
# 两层简单神经网络（全连接）
import tensorflow as tf
# 定义输入和参数
# 用placeholder实现输入定义 （sess.run中喂一组数据）
x = tf.placeholder(tf.float32, shape=(1, 2))
# 定义第一层神经网络(2个随机正态分布参数，3个节点--参数标准差是2，随机种子是1,如果没有特殊要求标准差、均值、随机种子是可以不写的)
w1 = tf.Variable(tf.random_normal([2, 3], stddev=1, seed=1))
# 定义第二层神经网络(3个节点，1个结果)
w2 = tf.Variable(tf.random_normal([3, 1], stddev=1, seed=1))
 
# 定义前向传播过程
a = tf.matmul(x, w1)
y = tf.matmul(a, w2)
 
# 用会话计算结果
with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    print("y is:\n", sess.run(y, feed_dict={x: [[0.7, 0.5]]}))
