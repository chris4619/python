'''
Created on 2019年5月8日

@author: YangYang
'''
# -*- coding: utf-8 -*-

from datetime import datetime

with open('file.txt', 'w') as f:
    f.write('今天是 ')
    f.write(datetime.now().strftime('%Y-%m-%d'))

with open('file.txt', 'r') as f:
    s = f.read()
    print('open for read...')
    print(s)

with open('file.txt', 'rb') as f:
    s = f.read()
    print('open as binary for read...')
    print(s)
