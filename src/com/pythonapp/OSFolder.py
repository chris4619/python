'''
Created on 2019年5月8日

@author: YangYang
'''
# -*- coding: utf-8 -*-

import os

# 查看当前目录的绝对路径:
os.path.abspath('.')

# 在某个目录下创建一个新目录，首先把新目录的完整路径表示出来:
os.path.join('/Users/michael', 'testdir')

# 然后创建一个目录:
os.mkdir('/Users/michael/testdir')

# 删掉一个目录:
os.rmdir('/Users/michael/testdir')
