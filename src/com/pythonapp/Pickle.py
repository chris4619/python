'''
Created on 2019年5月9日

@author: YangYang
'''
# -*- coding: utf-8 -*-

import pickle

d = dict(name='Bob', age=20, score=88)
# 序列化字典
data = pickle.dumps(d)
print(data)
# 反序列化为字典
reborn = pickle.loads(data)
print(reborn)
