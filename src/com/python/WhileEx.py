'''
Created on 2019年4月18日

@author: YangYang
'''
# -*- coding: utf-8 -*-

# 计算1+2+3+...+100:
sumint = 0
n = 1
while n <= 100:
    sumint = sumint + n
    n = n + 1
print(sumint)

# 计算1x2x3x...x100:
acc = 1
n = 1
while n <= 100:
    acc = acc * n
    n = n + 1
print(acc)
