'''
Created on 2019年4月18日

@author: YangYang
'''
# -*- coding: utf-8 -*-


def lazy_sum(*args):

    def sumint():
        ax = 0
        for n in args:
            ax = ax + n
        return ax

    return sumint


f = lazy_sum(1, 2, 4, 5, 7, 8, 9)
print(f)
print(f())


# why f1(), f2(), f3() returns 9, 9, 9 rather than 1, 4, 9?
def count():
    fs = []
    for i in range(1, 4):

        def f():
            return i * i

        fs.append(f)
    return fs


f1, f2, f3 = count()

print(f1())
print(f2())
print(f3())


# fix:
def count2():
    fs = []

    def f(n):

        def j():
            return n * n

        return j

    for i in range(1, 4):
        fs.append(f(i))
    return fs


f1, f2, f3 = count2()

print(f1())
print(f2())
print(f3())
